## TODO list

- [x] sophisticated GUI
- [x] collision detection
- [x] jump
- [x] FPS monitor
- [x] Resource => Asset


### Refactoring
- [ ] Extract all common logic from level for easier development
- [ ] Promises to async/await


### Graphics
- [x] Graphics optimisations (do not render tiles out of viewport)
- [x] Tile edges (tile grid is sometimes visible)
- [ ] **Move to WebGL**
- [ ] Background tile layers
- [ ] Multiple bg tile layers
- [ ] BG tile layers parallax

### Mechanics
- [ ] attack
- [ ] enemies
- [ ] dumb enemy AI
- [ ] top-side-only collision box (jump-through platforms)
- [ ] tweak jumping and falling


- [ ] redraw sand brick tiles


- [ ] sounds and music
- [ ] funky fonts