const { merge } = require('webpack-merge');
const parts = require('./webpack.parts');
const ErrorOverlayWebpackPlugin = require('error-overlay-webpack-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');

exports.default = merge(
    parts.defineEnv('develop'),
    parts.loadCss(),
    parts.devServer({
        host: process.env.DEV_HOST,
        port: process.env.DEV_PORT,
    }),
    {
        devtool: 'cheap-module-source-map',
        plugins: [
            new ErrorOverlayWebpackPlugin(),
            new StyleLintPlugin({
                context: './src',
            }),
        ],
        output: {
            publicPath: `http://${process.env.DEV_HOST}:${process.env.DEV_PORT}/`,
        },
    },
);
