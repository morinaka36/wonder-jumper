const { merge } = require('webpack-merge');
const parts = require('./webpack.parts');

exports.default = merge(
    parts.defineEnv('prod'),
    parts.extractCss({ use: ['css-loader', 'resolve-url-loader', 'sass-loader'] }),
    {
        optimization: {
            splitChunks: {
                cacheGroups: {
                    commons: {
                        test: /[\\/]node_modules[\\/]/,
                        name: 'vendor',
                        chunks: 'initial',
                    },
                },
            },
        },
    },
);
