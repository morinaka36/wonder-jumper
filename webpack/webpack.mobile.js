const { merge } = require('webpack-merge');

exports.default = merge({
    output: {
        publicPath: '/',
    },
});
