const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const webpack = require('webpack');

exports.devServer = ({ host, port } = {}) => ({
    devServer: {
        stats: 'errors-warnings',
        host,
        port,
        open: false,
        overlay: true,
    },
});

exports.loadCss = ({ include, exclude } = {}) => ({
    module: {
        rules: [
            {
                test: /\.s[ca]ss$/i,
                include,
                exclude,

                use: [
                    'style-loader',
                    'css-loader?sourceMap',
                    'resolve-url-loader',
                    'sass-loader',
                    {
                        loader: 'postcss-loader',
                        options: {
                            postcssOptions: {
                                plugins: [require('autoprefixer'), require('precss')],
                            },
                        },
                    },
                ],
            },
        ],
    },
});

exports.extractCss = ({ include, exclude, use = [] } = {}) => {
    const plugin = new MiniCssExtractPlugin({
        filename: '[name].css',
    });

    return {
        module: {
            rules: [
                {
                    test: /\.s[ca]ss$/i,
                    include,
                    exclude,

                    use: [MiniCssExtractPlugin.loader].concat(use),
                },
            ],
        },
        plugins: [plugin],
    };
};

exports.loadImages = ({ include, exclude, options } = {}) => ({
    module: {
        rules: [
            {
                test: /\.(jpg|png)$/,
                include,
                exclude,
                use: {
                    loader: 'file-loader',
                    options,
                },
            },
        ],
    },
});

exports.loadFonts = () => ({
    module: {
        rules: [
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'fonts/',
                        },
                    },
                ],
            },
        ],
    },
});

exports.loadJs = ({ include, exclude } = {}) => ({
    module: {
        rules: [
            {
                test: /\.js$/,
                include,
                exclude,
                use: ['babel-loader', 'eslint-loader'],
            },
        ],
    },
});

exports.loadTs = () => ({
    module: {
        rules: [
            {
                test: /\.(ts|tsx)$/,
                enforce: 'pre',
                use: [
                    {
                        options: {
                            eslintPath: require.resolve('eslint'),
                        },
                        loader: require.resolve('eslint-loader'),
                    },
                ],
                exclude: /node_modules/,
            },

            {
                test: /\.tsx?$/,
                use: [
                    {
                        loader: 'ts-loader',
                        options: {
                            appendTsSuffixTo: [/\.vue$/],
                        },
                    },
                ],
            },
        ],
    },
});

exports.loadVue = () => ({
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: [
                    {
                        loader: 'vue-loader',
                        options: {
                            esModule: true,
                            loaders: {
                                js: 'ts-loader',
                                scss: ['vue-style-loader', 'css-loader', 'resolve-url-loader', 'sass-loader'],
                            },
                        },
                    },
                ],
            },
        ],
    },
    plugins: [new VueLoaderPlugin()],
});

exports.clean = () => ({
    plugins: [new CleanWebpackPlugin()],
});

exports.defineEnv = (env) => ({
    plugins: [new webpack.DefinePlugin({ __CONFIG__: JSON.stringify({ main: buildMainConfig(env) }) })],
});

function buildMainConfig(env) {
    if (!env) {
        throw new Error('Please provide proper env');
    }

    const mainConfig = require('../config/main.json');
    let envConfig;

    try {
        envConfig = require(`../config/main-${env}.json`);
    } catch (e) {
        envConfig = {};
    }

    return Object.assign(mainConfig, envConfig, { env });
}
