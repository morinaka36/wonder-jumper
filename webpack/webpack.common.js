const { merge } = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const paths = require('./consts').PATH;
const parts = require('./webpack.parts');

exports.default = merge(
    parts.loadTs(),
    parts.loadVue(),
    parts.loadFonts(),
    parts.loadImages({
        options: {
            limit: 15000,
            name: '[name].[ext]',
        },
    }),

    parts.clean(),
    {
        entry: './src/main.ts',
        output: {
            filename: 'main.[chunkhash].js',
        },

        resolve: {
            alias: {
                vue$: 'vue/dist/vue.esm.js',
                '@': paths.src,
            },
            extensions: ['.web.js', '.ts', '.tsx', '.js', '.vue'],
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: './src/index.html',
            }),
            new CopyPlugin({ patterns: ['./static'] }),
        ],
    },
);
