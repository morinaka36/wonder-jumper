const path = require('path');

module.exports = {
    PATH: {
        app: path.join(__dirname, '../'),
        src: path.join(__dirname, '../src'),
    },
};
