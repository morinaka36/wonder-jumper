import 'reflect-metadata';
import './di';
import Vue, { VNode } from 'vue';
import App from './App.vue';

require('@/assets/styles/main.scss');

new Vue({
    el: '#app',
    render: (h): VNode => h(App),
});
