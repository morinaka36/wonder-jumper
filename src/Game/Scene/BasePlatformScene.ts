import { Scene } from '../../GameEngine/Scene';
import { Game } from '../../GameEngine/Game';
import { InGameDebug } from '../UI/InGameDebug';
import { Loading } from '../UI/Loading';
import { AssetRepository } from '../../GameEngine/Asset/AssetRepository';
import { TiledMapParser } from '../../GameEngine/TiledMap/TiledMapParser';
import { Display } from '../../GameEngine/Graphics/Display';
import { Player } from '../Object/Player';
import { GameObject } from '../../GameEngine/Object/GameObject';
import { container, injectable } from 'tsyringe';
import { ObjectType } from '../../Type/ObjectType';
import { Enemy } from '../Object/Enemy';
import { GameObjectRepository } from '../../GameEngine/Object/GameObjectRepository';
import { Tile } from '../Object/Tile';
import { GameLoop } from '../../GameEngine/GameLoop';
import { GameOver } from '../UI/GameOver';
import { Camera } from '../../GameEngine/Camera/Camera';

@injectable()
export class BasePlatformScene implements Scene {
    private _map?: TiledMapParser;

    private _player?: Player;

    private enemies: Enemy[] = [];

    private _isActive = false;

    private camera: Camera = container.resolve(Camera);

    private readonly game: Game;

    constructor(
        private readonly assets: AssetRepository,
        private readonly display: Display,
        private readonly objectRepository: GameObjectRepository,
        private readonly gameLoop: GameLoop,
    ) {
        this.game = container.resolve(Game);
    }

    private get map(): TiledMapParser {
        if (!this._map) {
            throw new Error('Map was not loaded');
        }

        return this._map;
    }

    private get player(): Player {
        if (this._player) {
            return this._player;
        }

        const playerObjects = this.objectRepository.get<Player>(ObjectType.SPAWN_PLAYER);

        if (!playerObjects) {
            throw new Error('Please define spawn_player object on map');
        }

        return (this._player = playerObjects[0]);
    }

    get isActive(): boolean {
        return this._isActive;
    }

    start(): void {
        this.game.setUI(new Loading());

        TiledMapParser.loadMap('inception')
            .then((tiledMap) => {
                this._map = tiledMap;

                this.map.processMap();

                this.stageAssets();

                return this.assets.performLoad();
            })
            .then(() => this.onAssetsReady())
            .catch((err) => {
                console.error(err);
                this.game.stopScene();
            });
    }

    stop(): void {
        this._isActive = false;
        this.objectRepository.reset();
        this.gameLoop.stop();
    }

    loopUpdate(dt: number): void {
        this.player.update(dt);

        if (this.player.isDead) {
            this.game.setUI(new GameOver());

            this.game.stopScene();
        }

        this.enemies.forEach((object: GameObject) => object.update(dt));

        this.render();
    }

    private onAssetsReady(): void {
        this.game.setUI(new InGameDebug(this.game));

        this.enemies = this.objectRepository.get<Enemy>(ObjectType.SPAWN_ENEMY);

        this._isActive = true;

        this.camera.setTrackTarget(this.player);

        this.gameLoop.registerUpdateTarget(this).start();
    }

    private render(): void {
        this.display.stageObjects(...this.objectRepository.get<Tile>(ObjectType.TILE), ...this.enemies, this.player);

        if (this.game.debug) {
            this.display.stageObjects(
                ...this.objectRepository.get(ObjectType.COLLISION),
                ...this.objectRepository.get(ObjectType.DEATH),
            );
        }

        this.display.render();
    }

    private stageAssets(): void {
        // Nothing yet
    }
}
