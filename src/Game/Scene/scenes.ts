import { SceneInfo } from '../../Type/SceneInfo';
import { BasePlatformScene } from './BasePlatformScene';

export const scenes: SceneInfo[] = [
    {
        id: 1,
        title: 'Level 1 - Collision detection playground',
        class: BasePlatformScene,
    },
];
