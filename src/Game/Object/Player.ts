import { GameObject } from '../../GameEngine/Object/GameObject';
import { Input, KeyName } from '../../GameEngine/Input';
import { Point2D } from '../../Type/Point2D';
import { Size } from '../../Type/Size';
import { Sprite } from '../../GameEngine/Sprite/Sprite';
import { container } from 'tsyringe';
import { TiledMapType, TiledObject } from 'tiled-types';
import { AssetRepository } from '../../GameEngine/Asset/AssetRepository';
import { ObjectType } from '../../Type/ObjectType';
import { GameObjectRepository } from '../../GameEngine/Object/GameObjectRepository';
import { Renderer } from '../../GameEngine/Graphics/Renderer';
import { SpriteRenderer } from '../../GameEngine/Graphics/Renderer/SpriteRenderer';
import { Velocity } from '../../Type/Velocity';
import { GameObjectRenderable } from '../../GameEngine/Object/GameObjectRenderable';
import { StaticSprite } from '../../GameEngine/Sprite/StaticSprite';
import { CameraTrackable } from '../../GameEngine/Camera/Camera';

export enum PlayerState {
    IDLE = 'idle',
    RUNNING = 'running',
    JUMPING = 'jumping',
    FALLING = 'falling',
}

export class Player extends GameObjectRenderable implements CameraTrackable {
    private readonly friction = 0.78;
    private readonly maxVelocity = 2 / this.friction;
    private readonly gravity = 3;
    private readonly jumpEasing = 1.1;
    private readonly jumpTimeMax = 0.3;
    private readonly jumpVelocityMax = 8;

    private objectRepository!: GameObjectRepository;

    private input!: Input;

    private state: PlayerState = PlayerState.FALLING;

    private velocity: Velocity = { x: 0, y: 0 };

    private jumpTime = 0;

    private _isDead = false;

    get isDead(): boolean {
        return this._isDead;
    }

    constructor(position: Point2D, size: Size, sprite: Sprite) {
        super(position, size, sprite);

        this.objectRepository = container.resolve(GameObjectRepository);
        this.input = container.resolve(Input);
    }

    static fromConfig(config: TiledObject<TiledMapType>): Player {
        container.resolve(AssetRepository).stageImage('warrior', import('../../assets/images/warrior.png'));

        return new Player(
            { x: config.x, y: config.y },
            { w: 32, h: 32 },
            new StaticSprite('warrior', { w: 32, h: 32 }),
        );
    }

    update(dt: number): void {
        this.move(dt);

        this.checkCollisions();

        this.checkDeath();
    }

    getRenderer(): Renderer {
        return new SpriteRenderer(this.position, this.size, this.renderable);
    }

    private move(dt: number): void {
        const velocityX = dt * 100;
        let newState = this.state;

        if (this.input.isDown(KeyName.RIGHT)) {
            this.velocity.x += velocityX;
            if (!this.inAir()) {
                newState = PlayerState.RUNNING;
            }
        }

        if (this.input.isDown(KeyName.LEFT)) {
            this.velocity.x -= velocityX;
            if (!this.inAir()) {
                newState = PlayerState.RUNNING;
            }
        }

        if (Math.abs(this.velocity.x) < 0.2) {
            this.velocity.x = 0;
            newState = PlayerState.IDLE;
        }

        if (this.input.isDown(KeyName.UP) && this.state !== PlayerState.FALLING) {
            this.velocity.y =
                (this.velocity.y > 0 ? -this.jumpVelocityMax : this.velocity.y * this.jumpEasing) - this.gravity;

            newState = this.jumpTime > this.jumpTimeMax ? PlayerState.FALLING : PlayerState.JUMPING;

            this.jumpTime += dt;
        } else if (this.inAir()) {
            newState = PlayerState.FALLING;
        }

        this.velocity.x *= this.friction;
        this.velocity.y += this.gravity;
        this.velocity.y *= this.friction;

        this.velocity = {
            x: Math.max(-this.maxVelocity, Math.min(this.velocity.x, this.maxVelocity)),
            // FIXME problem: dt * 100 on phone is bigger than maxSpeed
            y: Math.min(this.velocity.y, this.gravity),
        };

        if (newState) {
            this.state = newState;
        }
    }

    private onLanding(): void {
        if (this.state !== PlayerState.RUNNING) {
            this.state = PlayerState.IDLE;
        }

        this.jumpTime = 0;
    }

    private onDeath(): void {
        this._isDead = true;
    }

    private inAir(): boolean {
        return [PlayerState.JUMPING, PlayerState.FALLING].includes(this.state);
    }

    /**
     * Steps:
     * 1. Apply velocity
     * 2. Detect collisions
     * 3. Reapply x velocity only
     * 4. Fix x collisions
     * 5. Reapply y velocity
     * 6. Fix y collisions
     */
    private checkCollisions(): void {
        this.position.x += this.velocity.x;
        this.position.y += this.velocity.y;
        let playerRect = this.getRect();

        const collided = this.objectRepository
            .get<GameObject>(ObjectType.COLLISION)
            .filter((object) => object.getRect().collides(playerRect));

        this.position.y -= this.velocity.y;
        playerRect = this.getRect();

        collided.forEach((object) => {
            const objectRect = object.getRect();

            if (playerRect.collidesLeft(objectRect)) {
                this.position.x = playerRect.x = objectRect.r + 1;
            }
            if (playerRect.collidesRight(objectRect)) {
                this.position.x = playerRect.x = objectRect.x - this.size.w;
            }
        });

        this.position.y += this.velocity.y;
        playerRect = this.getRect();
        collided.forEach((object) => {
            const objectRect = object.getRect();

            if (playerRect.collidesBottom(objectRect)) {
                this.position.y = playerRect.y = objectRect.y - playerRect.h;

                if (this.inAir()) {
                    this.onLanding();
                }
            }
            if (playerRect.collidesTop(objectRect)) {
                this.position.y = playerRect.y = objectRect.b;
            }
        });
    }

    private checkDeath(): void {
        const deathObjects = this.objectRepository.get(ObjectType.DEATH);
        const playerRect = this.getRect();

        for (const deathObj of deathObjects) {
            if (deathObj.getRect().collides(playerRect)) {
                this.onDeath();

                break;
            }
        }
    }
}
