import { Sprite } from '../../GameEngine/Sprite/Sprite';
import { container } from 'tsyringe';
import { AssetRepository } from '../../GameEngine/Asset/AssetRepository';
import { Renderer } from '../../GameEngine/Graphics/Renderer';
import { SpriteRenderer } from '../../GameEngine/Graphics/Renderer/SpriteRenderer';
import { StaticSprite } from '../../GameEngine/Sprite/StaticSprite';
import { Point2D } from '../../Type/Point2D';
import { Size } from '../../Type/Size';
import { GameObjectRenderable } from '../../GameEngine/Object/GameObjectRenderable';
import { TiledMapType, TiledObject } from 'tiled-types';

export class Enemy extends GameObjectRenderable {
    constructor(position: Point2D, size: Size, sprite: Sprite) {
        super(position, size, sprite);
    }

    static fromConfig(config: TiledObject<TiledMapType>): Enemy {
        container.resolve(AssetRepository).stageImage('enemy', import('../../assets/images/enemy.png'));

        return new Enemy({ x: config.x, y: config.y }, { w: 32, h: 32 }, new StaticSprite('enemy', { w: 32, h: 32 }));
    }

    getRenderer(): Renderer {
        return new SpriteRenderer(this.position, this.size, this.renderable);
    }
}
