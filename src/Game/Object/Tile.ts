import { GameObjectRenderable } from '../../GameEngine/Object/GameObjectRenderable';
import { SpriteRenderer } from '../../GameEngine/Graphics/Renderer/SpriteRenderer';

export class Tile extends GameObjectRenderable {
    getRenderer(): SpriteRenderer {
        return new SpriteRenderer(this.position, this.size, this.renderable);
    }
}
