import { UIController } from '../../GameEngine/UIController';
import IntroUI from '../../Component/UI/IntroUI.vue';
import { VueConstructor } from 'vue';
import { Game } from '../../GameEngine/Game';
import { SceneId, SceneInfo } from '../../Type/SceneInfo';
import { SceneRegistry } from '../SceneRegistry';
import { container, injectable } from 'tsyringe';

@injectable()
export class Intro implements UIController {
    public readonly vueComponent: VueConstructor = IntroUI;

    public readonly sceneRegistry: SceneRegistry = new SceneRegistry();

    public scenes: SceneInfo[] = this.sceneRegistry.getSceneList();

    public onSceneSelect(sceneId: SceneId): void {
        const scene = this.sceneRegistry.getSceneById(sceneId);

        if (scene) {
            container.resolve(Game).startScene(scene);
        }
    }
}
