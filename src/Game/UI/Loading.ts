import { UIController } from '../../GameEngine/UIController';
import { VueConstructor } from 'vue';
import LoadingUI from '../../Component/UI/LoadingUI.vue';

export class Loading implements UIController {
    public readonly vueComponent: VueConstructor = LoadingUI;
}
