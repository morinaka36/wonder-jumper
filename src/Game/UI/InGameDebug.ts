import { UIController } from '../../GameEngine/UIController';
import { VueConstructor } from 'vue';
import { Game } from '../../GameEngine/Game';
import InGameDebugUI from '../../Component/UI/InGameDebugUI.vue';
import { Intro } from './Intro';
import { injectable } from 'tsyringe';

@injectable()
export class InGameDebug implements UIController {
    public readonly vueComponent: VueConstructor = InGameDebugUI;

    public constructor(public readonly game: Game) {}

    public goHome(): void {
        this.game.stopScene();

        this.game.setUI(new Intro());
    }
}
