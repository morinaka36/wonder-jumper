import { UIController } from '../../GameEngine/UIController';
import { VueConstructor } from 'vue';
import { container, injectable } from 'tsyringe';
import GameOverUI from '../../Component/UI/GameOverUI.vue';
import { Game } from '../../GameEngine/Game';
import { Intro } from './Intro';

@injectable()
export class GameOver implements UIController {
    public readonly vueComponent: VueConstructor = GameOverUI;
    private readonly game = container.resolve(Game);

    retryLevel(): void {
        this.game.restartScene();
    }

    goToMenu(): void {
        this.game.setUI(new Intro());
    }
}
