import { SceneId, SceneInfo } from '../Type/SceneInfo';
import { scenes } from './Scene/scenes';

export class SceneRegistry {
    private scenes: SceneInfo[] = scenes;

    public getSceneById(id: SceneId): SceneInfo | null {
        return (
            this.scenes.find<SceneInfo>((scene: SceneInfo): scene is SceneInfo => {
                return scene.id === id;
            }) || null
        );
    }

    public getSceneList(): SceneInfo[] {
        return this.scenes;
    }
}
