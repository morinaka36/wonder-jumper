import { singleton } from 'tsyringe';
import { AppConfig } from '../../Type/AppConfig';

@singleton()
export class Config {
    public readonly config: AppConfig;

    constructor() {
        this.config = __CONFIG__.main;
    }

    get debug(): boolean {
        return this.config.debug;
    }
}
