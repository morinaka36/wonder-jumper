export enum LogLevel {
    INFO = 'info',
    ERROR = 'error',
    WARNING = 'warning',
}

export interface Logger {
    log(message: string, level: LogLevel, ctx?: Record<string, unknown>): void;

    info(message: string, ctx?: Record<string, unknown>): void;
    error(message: string, ctx?: Record<string, unknown>): void;
    warning(message: string, ctx?: Record<string, unknown>): void;
}
