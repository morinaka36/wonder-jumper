import { Logger, LogLevel } from './Logger';

export class ConsoleLogger implements Logger {
    log(message: string, level: LogLevel, ctx?: Record<string, unknown>): void {
        switch (level) {
            case LogLevel.ERROR:
                console.error(message, ctx);
                break;
            case LogLevel.INFO:
                console.info(message, ctx);
                break;
            case LogLevel.WARNING:
                console.warn(message, ctx);
                break;
        }
    }

    error(message: string, ctx?: Record<string, unknown>): void {
        this.log(message, LogLevel.ERROR, ctx);
    }

    info(message: string, ctx?: Record<string, unknown>): void {
        this.log(message, LogLevel.INFO, ctx);
    }

    warning(message: string, ctx?: Record<string, unknown>): void {
        this.log(message, LogLevel.WARNING, ctx);
    }
}
