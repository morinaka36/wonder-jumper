import { Logger } from './Logger';

export class DummyLogger implements Logger {
    log(): void {
        // Do nothing
    }

    error = this.log;
    info = this.log;
    warning = this.log;
}
