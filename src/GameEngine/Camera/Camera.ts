import { Point2D } from '../../Type/Point2D';
import { container, singleton } from 'tsyringe';
import { Rectangle } from '../Shape/Rectangle';
import { Canvas } from '../Graphics/Canvas/Canvas';

export interface CameraTrackable {
    getRect(): Rectangle;
}

@singleton()
export class Camera {
    private _target?: CameraTrackable;

    private targetRect?: Rectangle;

    private staticArea?: Rectangle;

    private renderOffset?: Point2D;

    private get target(): CameraTrackable {
        if (!this._target) {
            throw new Error('Set TrackTarget first');
        }

        return this._target;
    }

    setTrackTarget(t: CameraTrackable): Camera {
        this._target = t;

        return this;
    }

    getRenderOffset(): Rectangle {
        this.refresh();

        return this.renderOffset as Rectangle;
    }

    refresh(): void {
        const rect = this.target.getRect();
        if (this.targetRect && this.renderOffset && rect.equals(this.targetRect)) {
            return;
        }

        this.targetRect = rect;

        const staticArea = this.calculateStaticArea();
        if (this.staticArea && this.renderOffset && staticArea.equals(this.staticArea)) {
            return;
        }

        this.staticArea = staticArea;

        this.renderOffset = this.calculateOffset();
    }

    private calculateStaticArea(): Rectangle {
        const targetRect = this.target.getRect();

        let staticArea: Rectangle;

        if (!this.staticArea) {
            staticArea = new Rectangle();

            const paddingHor = 10;
            const paddingVer = 10;

            staticArea.x = targetRect.x - paddingHor;
            staticArea.y = targetRect.y - paddingVer;
            staticArea.w = targetRect.w + paddingHor * 2;
            staticArea.h = targetRect.h + paddingVer * 2;
        } else if (!targetRect.inside(this.staticArea)) {
            staticArea = this.staticArea.clone();

            if (targetRect.x < staticArea.x) {
                staticArea.x = targetRect.x;
            }
            if (targetRect.r > staticArea.r) {
                staticArea.x += targetRect.r - staticArea.r;
            }
            if (targetRect.y < staticArea.y) {
                staticArea.y = targetRect.y;
            }
            if (targetRect.b > staticArea.b) {
                staticArea.y += targetRect.b - staticArea.b;
            }
        } else {
            staticArea = this.staticArea;
        }

        return staticArea;
    }

    private calculateOffset(): Point2D {
        if (!this.staticArea) {
            this.staticArea = this.calculateStaticArea();
        }

        const canvas = container.resolve(Canvas).size;

        return {
            x: -(this.staticArea.x + this.staticArea.w / 2 - canvas.w / 2),
            y: -(this.staticArea.y + this.staticArea.h / 2 - canvas.h / 2),
        };
    }
}
