import { VueConstructor } from 'vue';
import { KeyName } from './Input';

export interface UIController {
    vueComponent: VueConstructor;

    onInput?(key: KeyName): void;
}
