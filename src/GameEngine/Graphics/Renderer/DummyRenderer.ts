import { AbstractRenderer } from './AbstractRenderer';
import { Canvas } from '../Canvas/Canvas';
import { Rectangle } from '../../Shape/Rectangle';

export class DummyRenderer extends AbstractRenderer {
    render(c: Canvas): void {
        if (!this.isDebug()) {
            return;
        }

        c.fillRect(new Rectangle(this.position.x, this.position.y, this.size.w, this.size.h), 'rgba(255, 0, 0, 0.1)');
    }
}
