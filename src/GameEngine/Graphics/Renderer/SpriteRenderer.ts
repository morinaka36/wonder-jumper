import { AbstractRenderer } from './AbstractRenderer';
import { Point2D } from '../../../Type/Point2D';
import { Size } from '../../../Type/Size';
import { Sprite } from '../../Sprite/Sprite';
import { Canvas } from '../Canvas/Canvas';
import { Rectangle } from '../../Shape/Rectangle';

export class SpriteRenderer extends AbstractRenderer {
    constructor(position: Point2D, size: Size, protected readonly sprite: Sprite) {
        super(position, size);
    }

    render(c: Canvas): void {
        if (this.isDebug()) {
            c.fillRect(new Rectangle(this.position.x, this.position.y, this.size.w, this.size.h), '#ff00fb');
        }

        const frame = this.sprite.getFrame();

        c.drawImage(frame.src, frame.position, frame.size, this.position, this.size);
    }
}
