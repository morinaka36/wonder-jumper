import { Point2D } from '../../../Type/Point2D';
import { Size } from '../../../Type/Size';
import { Renderer } from '../Renderer';
import { Rectangle } from '../../Shape/Rectangle';
import { container } from 'tsyringe';
import { Game } from '../../Game';
import { Canvas } from '../Canvas/Canvas';

export abstract class AbstractRenderer implements Renderer {
    protected position: Point2D;

    protected size: Size;

    constructor(position: Point2D, size: Size) {
        // Prevent gameObject mutation
        this.size = { ...size };
        this.position = { ...position };
    }

    getRect(): Rectangle {
        return new Rectangle(this.position.x, this.position.y, this.size.w, this.size.h);
    }

    setOffset(offset: Point2D): Renderer {
        this.position.x = this.round(this.position.x + offset.x);
        this.position.y = this.round(this.position.y + offset.y);

        return this;
    }

    protected isDebug(): boolean {
        return container.resolve(Game).debug;
    }

    protected round(n: number): number {
        return Math.floor(n);
    }

    abstract render(c: Canvas): void;
}
