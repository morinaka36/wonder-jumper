import { Point2D } from '../../Type/Point2D';
import { Rectangle } from '../Shape/Rectangle';
import { Canvas } from './Canvas/Canvas';

export interface Renderer {
    setOffset(offset: Point2D): Renderer;

    render(c: Canvas): void;

    getRect(): Rectangle;
}
