import { container, singleton } from 'tsyringe';
import { Size } from '../../../Type/Size';
import { ContextAdapter } from './ContextAdapter';
import { ContextAdapter2D } from './ContextAdapter2D';
import { ContextAdapterWebGL } from './ContextAdapterWebGL';
import { Rectangle } from '../../Shape/Rectangle';
import { Point2D } from '../../../Type/Point2D';
import { Logger } from '../../Util/Logger/Logger';

export type FillStyle = string;

@singleton()
export class Canvas implements ContextAdapter {
    private readonly ctx: ContextAdapter;

    private readonly logger: Logger = container.resolve('Logger');

    get size(): Size {
        return { w: this.el.width, h: this.el.height };
    }

    constructor(private readonly el: HTMLCanvasElement) {
        this.ctx = this.detectRenderingCtx();

        this.logger.info('Canvas mounted', { ctx: this.ctx });
    }

    fillRect(r: Rectangle, fillStyle: FillStyle): void {
        this.ctx.fillRect(r, fillStyle);
    }

    drawImage(img: HTMLImageElement, position: Point2D, size?: Size, sPosition?: Point2D, sSize?: Size): void {
        this.ctx.drawImage(img, position, size, sPosition, sSize);
    }

    private detectRenderingCtx(): ContextAdapter {
        let adapter: ContextAdapter | undefined;

        const webgl = this.el.getContext('webgl');
        if (webgl) {
            adapter = new ContextAdapterWebGL(webgl);
        }

        const webglExp = this.el.getContext('experimental-webgl');
        if (webglExp) {
            adapter = new ContextAdapterWebGL(webglExp as WebGLRenderingContext);
        }

        const ctx2d = this.el.getContext('2d');
        if (ctx2d) {
            adapter = new ContextAdapter2D(ctx2d);
        }

        if (!adapter) {
            throw new Error('Could not find compatible rendering context');
        }

        return adapter;
    }
}
