import { Rectangle } from '../../Shape/Rectangle';
import { FillStyle } from './Canvas';
import { Point2D } from '../../../Type/Point2D';
import { Size } from '../../../Type/Size';

export interface ContextAdapter {
    fillRect(r: Rectangle, fillStyle: FillStyle): void;

    drawImage(img: HTMLImageElement, position: Point2D, size?: Size, sPosition?: Point2D, sSize?: Size): void;
}
