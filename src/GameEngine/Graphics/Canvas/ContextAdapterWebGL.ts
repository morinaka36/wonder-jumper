import { ContextAdapter } from './ContextAdapter';

export class ContextAdapterWebGL implements ContextAdapter {
    constructor(private readonly ctx: WebGLRenderingContext) {}

    fillRect(): void {
        // TODO
    }

    drawImage(): void {
        // TODO
    }
}
