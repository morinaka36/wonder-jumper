import { GameObject } from '../Object/GameObject';
import { container, singleton } from 'tsyringe';
import { Rectangle } from '../Shape/Rectangle';
import { Camera } from '../Camera/Camera';
import { Point2D } from '../../Type/Point2D';
import { Canvas } from './Canvas/Canvas';

interface DisplayStats {
    objects: {
        total: number;
        rendered: number;
        skipped: number;
    };
}

@singleton()
export class Display {
    private camera: Camera = container.resolve(Camera);

    private canvas: Canvas = container.resolve(Canvas);

    private _stats: DisplayStats = Display.initStats();

    private stagedObjects: GameObject[] = [];

    private _viewport?: Rectangle;

    get stats(): DisplayStats {
        return this._stats;
    }

    private get viewport(): Rectangle {
        if (this._viewport) {
            return this._viewport;
        }

        const canvasSize = this.canvas.size;

        return (this._viewport = new Rectangle(0, 0, canvasSize.w, canvasSize.h));
    }

    clearScreen(color = 'black'): Display {
        const canvasSize = this.canvas.size;
        this.canvas.fillRect(new Rectangle(0, 0, canvasSize.w, canvasSize.h), color);

        return this;
    }

    stageObjects(...objects: GameObject[]): Display {
        this.stagedObjects.push(...objects);

        return this;
    }

    render(): void {
        this._stats = Display.initStats();

        this.clearScreen();

        const offset = this.camera.getRenderOffset();

        this.stagedObjects.forEach((object) => this.renderObject(object, offset));

        this.stagedObjects = [];
    }

    private renderObject(object: GameObject, offset: Point2D): void {
        this._stats.objects.total++;

        const renderer = object.getRenderer().setOffset(offset);

        if (!renderer.getRect().collides(this.viewport)) {
            this._stats.objects.skipped++;

            return;
        }

        renderer.render(this.canvas);

        this._stats.objects.rendered++;
    }

    private static initStats(): DisplayStats {
        return {
            objects: {
                rendered: 0,
                skipped: 0,
                total: 0,
            },
        };
    }
}
