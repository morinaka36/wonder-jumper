import { Point2D } from '../../Type/Point2D';
import { Size } from '../../Type/Size';
import { Rectangle } from '../Shape/Rectangle';
import { TiledMapType, TiledObject } from 'tiled-types';
import { Renderer } from '../Graphics/Renderer';
import { DummyRenderer } from '../Graphics/Renderer/DummyRenderer';

export class GameObject {
    constructor(public position: Point2D, public size: Size) {}

    static fromConfig(config: TiledObject<TiledMapType>): GameObject {
        return new GameObject({ x: config.x, y: config.y }, { w: config.width, h: config.height });
    }

    static fromRectangle(r: Rectangle): GameObject {
        return new GameObject(r, r);
    }

    getRenderer(): Renderer {
        return new DummyRenderer(this.position, this.size);
    }

    getRect(): Rectangle {
        return new Rectangle(this.position.x, this.position.y, this.size.w, this.size.h);
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    update(dt: number): void {
        // Do nothing
    }
}
