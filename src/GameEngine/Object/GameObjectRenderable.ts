import { GameObject } from './GameObject';
import { Point2D } from '../../Type/Point2D';
import { Size } from '../../Type/Size';
import { Sprite } from '../Sprite/Sprite';

type Renderable = Sprite;

export class GameObjectRenderable extends GameObject {
    constructor(position: Point2D, size: Size, public readonly renderable: Renderable) {
        super(position, size);
    }
}
