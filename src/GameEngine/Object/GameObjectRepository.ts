import { GameObject } from './GameObject';
import { TiledMapType, TiledObject } from 'tiled-types';
import { ObjectType } from '../../Type/ObjectType';
import { singleton } from 'tsyringe';
import { Tile } from '../../Game/Object/Tile';
import { Player } from '../../Game/Object/Player';
import { Enemy } from '../../Game/Object/Enemy';
import { GameObjectRenderable } from './GameObjectRenderable';

type ObjectTypeMap = {
    [T in ObjectType]?: typeof GameObject | typeof GameObjectRenderable;
};

type ObjectsMap = {
    [T in ObjectType]?: GameObject[];
};

@singleton()
export class GameObjectRepository {
    private objectCache: ObjectsMap = {};

    private readonly objectTypeMap: ObjectTypeMap = {
        [ObjectType.SPAWN_PLAYER]: Player,
        [ObjectType.SPAWN_ENEMY]: Enemy,
    };

    addFromTiledObject(config: TiledObject<TiledMapType>): GameObjectRepository {
        const mappedType = GameObjectRepository.getMappedType(config);
        const objectType = this.objectTypeMap[mappedType] || GameObject;

        (this.objectCache[mappedType] = this.objectCache[mappedType] || []).push(objectType.fromConfig(config));

        return this;
    }

    addTile(object: Tile): GameObjectRepository {
        (this.objectCache[ObjectType.TILE] = this.objectCache[ObjectType.TILE] || []).push(object);

        return this;
    }

    get<T extends GameObject = GameObject>(type: ObjectType): T[] {
        return (this.objectCache[type] as T[]) || [];
    }

    reset(): void {
        this.objectCache = {};
    }

    private static getMappedType(config: TiledObject<TiledMapType>): ObjectType {
        const objType = config.type as ObjectType;

        return Object.values(ObjectType).includes(objType) ? objType : ObjectType.UNMAPPED;
    }
}
