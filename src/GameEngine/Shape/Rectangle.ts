export class Rectangle {
    constructor(public x = 0, public y = 0, public w = 0, public h = 0) {}

    get r(): number {
        return this.x + this.w;
    }

    get b(): number {
        return this.y + this.h;
    }

    collides(that: Rectangle): boolean {
        return !(this.r <= that.x || this.x > that.r || this.b <= that.y || this.y > that.b);
    }

    collidesBottom(that: Rectangle): boolean {
        return !(this.b <= that.y || this.y > that.y || this.r <= that.x || this.x > that.r);
    }

    collidesTop(that: Rectangle): boolean {
        return !(this.y >= that.b || this.b < that.b || this.r <= that.x || this.x > that.r);
    }

    collidesLeft(that: Rectangle): boolean {
        return !(this.b <= that.y || this.y >= that.b || this.x > that.r || this.r <= that.r);
    }

    collidesRight(that: Rectangle): boolean {
        return !(this.b <= that.y || this.y >= that.b || this.r < that.x || this.x >= that.x);
    }

    inside(that: Rectangle): boolean {
        return !(this.x < that.x || this.y < that.y || this.r > that.r || this.b > that.b);
    }

    equals(that: Rectangle): boolean {
        return JSON.stringify(this) === JSON.stringify(that);
    }

    clone(): Rectangle {
        return new Rectangle(this.x, this.y, this.w, this.h);
    }
}
