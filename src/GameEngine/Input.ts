import { singleton } from 'tsyringe';

export enum KeyName {
    UP = 'up',
    DOWN = 'down',
    LEFT = 'left',
    RIGHT = 'right',
    CENTER = 'center',
    SOFT_LEFT = 'soft_left',
    SOFT_RIGHT = 'soft_right',
}

export enum KeyState {
    UP = 'up',
    DOWN = 'down',
}

type InputKeyState = { [key in KeyName]?: KeyState };

@singleton()
export class Input {
    private pressedKeys: InputKeyState = {};

    public handleInput(event: KeyboardEvent, keyState: KeyState): KeyName | null {
        let keyName: KeyName | null;

        switch (event.code) {
            case 'Digit2':
            case 'ArrowUp':
            case 'KeyW':
                keyName = KeyName.UP;
                break;
            case 'Digit4':
            case 'ArrowLeft':
            case 'KeyA':
                keyName = KeyName.LEFT;
                break;
            case 'Digit6':
            case 'ArrowRight':
            case 'KeyD':
                keyName = KeyName.RIGHT;
                break;
            case 'Digit8':
            case 'ArrowDown':
            case 'KeyS':
                keyName = KeyName.DOWN;
                break;
            case 'Digit5':
            case 'Space':
                keyName = KeyName.CENTER;
                break;
            default:
                keyName = null;
        }

        if (!keyName) {
            switch (event.key) {
                case 'Enter':
                    keyName = KeyName.CENTER;
                    break;
                case 'SoftLeft':
                    keyName = KeyName.SOFT_LEFT;
                    break;
                case 'SoftRight':
                    keyName = KeyName.SOFT_RIGHT;
                    break;
            }
        }

        if (keyName) {
            this.pressedKeys[keyName] = keyState;
        }

        return keyName;
    }

    public reset(): void {
        this.pressedKeys = {};
    }

    public isDown(key: KeyName): boolean {
        if (!(key in this.pressedKeys)) {
            return false;
        }

        return this.pressedKeys[key] === KeyState.DOWN;
    }
}
