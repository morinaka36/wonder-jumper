import { LoopUpdated } from '../Type/LoopUpdated';

export interface Scene extends LoopUpdated {
    start(): void;

    stop(): void;
}
