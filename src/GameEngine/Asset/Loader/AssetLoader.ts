import { CacheableAsset, Asset } from '../AssetRepository';

export interface AssetLoader {
    load(res: Asset): Promise<CacheableAsset>;
}
