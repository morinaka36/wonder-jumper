import { Asset } from '../AssetRepository';
import { AssetLoader } from './AssetLoader';

export class ImageLoader implements AssetLoader {
    load(res: Asset): Promise<HTMLImageElement> {
        return new Promise((resolve, reject) => {
            if (res instanceof HTMLImageElement) {
                resolve(res);
            }

            if (res instanceof Promise) {
                res.then((val) => {
                    const img = new Image();

                    img.src = val.default;

                    img.onload = (): void => resolve(img);
                    img.onerror = (e): void => reject(e);
                });
            } else {
                reject('Loader is not compatible with given asset');
            }
        });
    }
}
