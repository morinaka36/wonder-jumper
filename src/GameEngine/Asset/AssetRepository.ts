import { container, singleton } from 'tsyringe';
import { AssetLoader } from './Loader/AssetLoader';
import { ImageLoader } from './Loader/ImageLoader';
import { Logger } from '../Util/Logger/Logger';

export type CacheableAsset = HTMLImageElement | HTMLAudioElement;

export type Asset = CacheableAsset | Promise<{ default: string }>;

export type AssetLoadConfig = {
    src: Asset;
    name: string;
    loader: AssetLoader;
};

@singleton()
export class AssetRepository {
    private readonly logger: Logger = container.resolve<Logger>('Logger');

    private assets = new Map<string, CacheableAsset>();

    private stagedAssets = new Map<string, AssetLoadConfig>();

    isReady(): boolean {
        return !this.stagedAssets.size;
    }

    stage(...assets: AssetLoadConfig[]): this {
        for (const asset of assets) {
            if (asset.name in this.assets || asset.name in this.stagedAssets) {
                throw new Error(`Asset ${asset.name} already registered in ${AssetRepository.name}`);
            }

            this.stagedAssets.set(asset.name, asset);
        }

        return this;
    }

    stageImage(name: string, src: Asset): this {
        return this.stage({
            name,
            src,
            loader: new ImageLoader(),
        });
    }

    get<T extends CacheableAsset>(name: string): T {
        const asset = this.assets.get(name);

        if (!asset) {
            throw new Error(`Asset "${name}" was not found`);
        }

        return asset as T;
    }

    performLoad(): Promise<void[]> {
        const promises: Promise<void>[] = [];

        this.stagedAssets.forEach((loadConfig) => {
            const promise = loadConfig.loader
                .load(loadConfig.src)
                .then((asset) => {
                    this.assets.set(loadConfig.name, asset);
                    this.stagedAssets.delete(loadConfig.name);
                })
                .catch((e) => this.logger.error('Could not load asset', { loadConfig, e }));

            promises.push(promise);
        });

        return Promise.all(promises);
    }
}
