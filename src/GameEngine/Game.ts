import { Input, KeyName, KeyState } from './Input';
import { UIController } from './UIController';
import { Scene } from './Scene';
import { Intro } from '../Game/UI/Intro';
import { SceneInfo } from '../Type/SceneInfo';
import { container, injectable, singleton } from 'tsyringe';
import { Canvas } from './Graphics/Canvas/Canvas';

@injectable()
@singleton()
export class Game {
    readonly debug = true;

    currentUI: UIController = container.resolve(Intro);

    private scene?: Scene;

    private lastSceneInfo?: SceneInfo;

    constructor(private readonly input: Input) {}

    onCanvasMounted(canvas: HTMLCanvasElement): void {
        container.register(Canvas, { useValue: new Canvas(canvas) });
    }

    startScene(sceneInfo: SceneInfo): void {
        this.stopScene();

        const scene = container.resolve(sceneInfo.class);
        if (!scene) {
            throw new Error(`Scene #${sceneInfo.id} "${sceneInfo.title} was not found"`);
        }

        this.scene = scene;
        this.lastSceneInfo = sceneInfo;

        console.debug(`Starting scene #${sceneInfo.id}`);

        this.scene.start();
    }

    restartScene(): void {
        if (this.lastSceneInfo) {
            this.startScene(this.lastSceneInfo);
        }
    }

    stopScene(): void {
        this.scene?.stop();
        delete this.scene;
    }

    setUI(ui: UIController): void {
        this.currentUI = ui;
    }

    onInput(event: KeyboardEvent): KeyName | null {
        let inputKey: KeyName | null = null;

        switch (event.type) {
            case 'keyup':
                this.input.handleInput(event, KeyState.UP);
                break;

            case 'keydown':
                inputKey = this.input.handleInput(event, KeyState.DOWN);

                if (inputKey) {
                    event.preventDefault();

                    if (this.currentUI?.onInput) {
                        this.currentUI?.onInput(inputKey);
                    }
                }

                break;
        }

        return inputKey;
    }

    onBlur(): void {
        this.input.reset();
    }
}
