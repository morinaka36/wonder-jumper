import { Size } from '../../Type/Size';
import { Point2D } from '../../Type/Point2D';
import { SpriteSheetDirection } from '../../Type/SpriteSheetDirection';
import { SpriteFrame } from './SpriteFrame';
import { container } from 'tsyringe';
import { AssetRepository } from '../Asset/AssetRepository';

export type SpriteSource = HTMLImageElement | string;

export class Sprite {
    private spritesheet?: HTMLImageElement;

    private readonly spritesheetName?: string;

    public readonly size!: Size;

    private readonly sheetPosition: Point2D;

    private readonly sheetDirection: SpriteSheetDirection;

    private readonly framePlot: number[];

    private readonly speed: number;

    private readonly loop: boolean;

    private currFrame = 0;

    private finished = false;

    public static tile(spritesheet: SpriteSource, size: Size, sheetPosition: Point2D): Sprite {
        return new Sprite(spritesheet, size, sheetPosition, SpriteSheetDirection.HORIZONTAL, [0], 0, false);
    }

    constructor(
        spritesheet: SpriteSource,
        size: Size,
        sheetPosition: Point2D,
        sheetDirection: SpriteSheetDirection,
        framePlot: number[],
        speed: number,
        loop: boolean,
    ) {
        if (typeof spritesheet === 'string') {
            this.spritesheetName = spritesheet;
        } else {
            this.spritesheet = spritesheet;
        }

        this.size = size;
        this.sheetPosition = sheetPosition;
        this.framePlot = framePlot;
        this.sheetDirection = sheetDirection;
        this.speed = speed;
        this.loop = loop;
    }

    public setNextFrame(dt: number): Sprite {
        this.currFrame += this.speed * dt;

        const max = this.framePlot.length;

        if (!this.loop && this.currFrame >= max) {
            this.finished = true;
        }

        return this;
    }

    public getFrame(): SpriteFrame {
        if (!this.spritesheet) {
            if (this.spritesheetName) {
                this.spritesheet = container.resolve(AssetRepository).get<HTMLImageElement>(this.spritesheetName);
            } else {
                throw new Error();
            }
        }

        const position: Point2D = Object.assign({}, this.sheetPosition);

        const max = this.framePlot.length;
        const frame = this.framePlot[Math.floor(this.currFrame % max)];

        if (this.sheetDirection === SpriteSheetDirection.VERTICAL) {
            position.y += frame * this.size.h;
        } else {
            position.x += frame * this.size.w;
        }

        return {
            src: this.spritesheet,
            size: this.size,
            position,
        };
    }

    public isFinished(): boolean {
        return this.finished;
    }
}
