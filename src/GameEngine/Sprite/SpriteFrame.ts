import { Size } from '../../Type/Size';
import { Point2D } from '../../Type/Point2D';

export interface SpriteFrame {
    src: HTMLImageElement;
    size: Size;
    position: Point2D;
}
