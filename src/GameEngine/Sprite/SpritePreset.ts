import { Size } from '../../Type/Size';
import { Point2D } from '../../Type/Point2D';
import { SpriteSheetDirection } from '../../Type/SpriteSheetDirection';

export abstract class SpritePreset {
    static defaultSpritesheet: HTMLImageElement | null;

    spritesheet!: HTMLImageElement;

    loop = true;
    sheetDirection: SpriteSheetDirection = SpriteSheetDirection.HORIZONTAL;

    size!: Size;
    sheetPosition!: Point2D;
    framePlot!: number[];
    speed!: number;

    constructor(spritesheet?: HTMLImageElement) {
        if (!spritesheet) {
            if (!SpritePreset.defaultSpritesheet) {
                throw new Error('Please provide spritesheet source or set SpritePreset.defaultSpritesheet');
            }

            this.spritesheet = SpritePreset.defaultSpritesheet;
        } else {
            this.spritesheet = spritesheet;
        }
    }
}
