import { Sprite, SpriteSource } from './Sprite';
import { Size } from '../../Type/Size';
import { SpriteSheetDirection } from '../../Type/SpriteSheetDirection';

export class StaticSprite extends Sprite {
    constructor(image: SpriteSource, size: Size) {
        super(image, size, { x: 0, y: 0 }, SpriteSheetDirection.HORIZONTAL, [0], 0, false);
    }
}
