import { singleton } from 'tsyringe';
import { LoopUpdated } from '../Type/LoopUpdated';

@singleton()
export class GameLoop {
    private lastUpdate?: number;

    private running = false;

    private updateTargets: LoopUpdated[] = [];

    private readonly maxFpsSamples = 10;

    private fpsSamples: number[] = [];

    private _fps = 60;

    get fps(): number {
        return this._fps;
    }

    registerUpdateTarget(target: LoopUpdated): GameLoop {
        this.updateTargets.push(target);

        return this;
    }

    start(): void {
        this.running = true;

        this.tick(Date.now());
    }

    stop(): void {
        this.running = false;
    }

    tick(now: number): void {
        if (!this.running) {
            return;
        }

        const elapsed = Math.min(60, Math.max(0, now - (this.lastUpdate || 0)));

        this.lastUpdate = now;

        this.updateFps(elapsed);

        this.updateTargets.filter((target) => {
            if (!target.isActive) {
                return false;
            }

            target.loopUpdate(elapsed / 1000);

            return true;
        });

        requestAnimationFrame((time) => this.tick(time));
    }

    private updateFps(elapsed: number): void {
        this.fpsSamples.push(Math.floor(1000 / elapsed));

        if (this.fpsSamples.length >= this.maxFpsSamples) {
            const sum = this.fpsSamples.reduce((acc, el) => acc + el);

            this._fps = Math.floor(sum / this.maxFpsSamples);

            this.fpsSamples = [];
        }
    }
}
