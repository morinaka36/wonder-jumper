import { TiledTileset } from 'tiled-types';
import { Point2D } from '../../Type/Point2D';
import { TiledMapParser } from './TiledMapParser';
import { Sprite } from '../Sprite/Sprite';

export class Tileset {
    private tilesOnImage: Record<number, Point2D> = {};

    public constructor(public readonly config: TiledTileset, private readonly mapParser: TiledMapParser) {
        const firstgid = this.config.firstgid;
        const lastgid = firstgid + this.config.tilecount - 1;

        for (let gid = firstgid; gid <= lastgid; gid++) {
            const row = Math.floor((gid - firstgid) / this.config.columns);
            const column = (gid - firstgid) % this.config.columns;

            const xPos = this.config.margin + (this.config.tilewidth + this.config.spacing) * column;
            const yPos = this.config.margin + (this.config.tileheight + this.config.spacing) * row;

            this.tilesOnImage[gid] = {
                x: xPos,
                y: yPos,
            };

            this.mapParser.setGIDTileset(gid, this);
        }
    }

    public readonly assetName = `tileset.${this.config.name}`;

    public getTileSprite(tileGID: number): Sprite {
        return Sprite.tile(
            this.assetName,
            { w: this.config.tilewidth, h: this.config.tileheight },
            this.tilesOnImage[tileGID],
        );
    }
}
