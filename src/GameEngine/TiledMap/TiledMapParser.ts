import { AssetRepository } from '../Asset/AssetRepository';
import TiledMap, {
    TiledLayerAbstract,
    TiledLayerObjectgroup,
    TiledLayerTilelayer,
    TiledLayerType,
    TiledMapType,
} from 'tiled-types';
import { Tileset } from './Tileset';
import { Point2D } from '../../Type/Point2D';
import { container } from 'tsyringe';
import { GameObjectRepository } from '../Object/GameObjectRepository';
import { ImageLoader } from '../Asset/Loader/ImageLoader';
import { Tile } from '../../Game/Object/Tile';

export class TiledMapParser {
    private readonly objectRepo: GameObjectRepository;

    private readonly assetRepository: AssetRepository = container.resolve(AssetRepository);

    private readonly tilesets = new Map<string, Tileset>();

    private gidIndex: Record<number, Tileset> = {};

    public constructor(private mapConfig: TiledMap) {
        this.objectRepo = container.resolve(GameObjectRepository);

        for (const tileset of this.mapConfig.tilesets) {
            this.tilesets.set(tileset.name, new Tileset(tileset, this));
        }
    }

    static async loadMap(map: string): Promise<TiledMapParser> {
        const config = (await import(`@/assets/maps/${map}.json`)) as TiledMap;

        return new TiledMapParser(config);
    }

    loadAssets(): void {
        this.tilesets.forEach((tileset) => {
            this.assetRepository.stage({
                name: tileset.assetName,
                src: import(`@/assets/maps/${tileset.config.image}`),
                loader: new ImageLoader(),
            });
        });
    }

    setGIDTileset(gid: number, tileset: Tileset): void {
        this.gidIndex[gid] = tileset;
    }

    processMap(): void {
        this.loadAssets();

        for (const layer of this.mapConfig.layers) {
            if (TiledMapParser.isObjectGroup(layer)) {
                this.processObjectGroup(layer);
            } else if (TiledMapParser.isTileLayer(layer) && layer.visible && layer.data) {
                this.processTileLayer(layer);
            }
        }
    }

    private processObjectGroup(layer: TiledLayerObjectgroup<TiledMapType>): void {
        layer.objects.map((object) => this.objectRepo.addFromTiledObject(object));
    }

    private processTileLayer(layer: TiledLayerTilelayer): void {
        layer.data?.forEach((gid, idx) => {
            if (gid === 0) {
                return;
            }

            const row = Math.floor(+idx / layer.width);
            const col = +idx % layer.width;

            const pos: Point2D = {
                x: layer.x + this.mapConfig.tilewidth * col,
                y: layer.y + this.mapConfig.tileheight * row,
            };

            const sprite = this.gidIndex[gid].getTileSprite(gid);

            this.objectRepo.addTile(new Tile(pos, sprite.size, sprite));
        });
    }

    private static isTileLayer(layer: TiledLayerAbstract<TiledLayerType>): layer is TiledLayerTilelayer {
        return layer.type === 'tilelayer';
    }

    private static isObjectGroup(
        layer: TiledLayerAbstract<TiledLayerType>,
    ): layer is TiledLayerObjectgroup<TiledMapType> {
        return layer.type === 'objectgroup';
    }
}
