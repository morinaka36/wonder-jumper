import { AppConfig } from './Type/AppConfig';

declare global {
    const __CONFIG__: { main: AppConfig };
}
