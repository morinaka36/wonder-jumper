// use this file for DI config
import { container, predicateAwareClassFactory } from 'tsyringe';
import { Logger } from './GameEngine/Util/Logger/Logger';
import { ConsoleLogger } from './GameEngine/Util/Logger/ConsoleLogger';
import { DummyLogger } from './GameEngine/Util/Logger/DummyLogger';
import { Config } from './GameEngine/Util/Config';

container.register('Logger', {
    useFactory: predicateAwareClassFactory<Logger>((c) => c.resolve(Config).debug, ConsoleLogger, DummyLogger),
});
