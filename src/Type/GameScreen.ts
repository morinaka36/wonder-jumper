export enum GameScreen {
    LOADING = 'loading',
    INTRO = 'intro',
    GAME = 'game',
    PAUSE = 'pause',
    GAME_OVER = 'game_over',
}
