export enum SpriteSheetDirection {
    HORIZONTAL = 'horizontal',
    VERTICAL = 'vertical',
}
