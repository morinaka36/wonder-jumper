export interface AppConfig {
    env: 'prod' | 'develop' | 'mobile';
    debug: boolean;
}
