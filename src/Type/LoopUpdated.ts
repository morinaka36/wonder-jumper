export interface LoopUpdated {
    isActive: boolean;

    loopUpdate(dt: number): void;
}
