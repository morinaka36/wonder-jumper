import { Scene } from '../GameEngine/Scene';
import { Constructor } from '../GameEngine/Constructor';

export type SceneInfo = {
    id: SceneId;
    title: string;
    class: Constructor<Scene>;
};

export type SceneId = number;
