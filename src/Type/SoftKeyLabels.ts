import { SoftKey } from './SoftKey';

export type SoftKeyLabels = {
    [Pos in SoftKey]: string;
};
