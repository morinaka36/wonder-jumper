export enum SoftKey {
    LEFT = 'left',
    CENTER = 'center',
    RIGHT = 'right',
}
