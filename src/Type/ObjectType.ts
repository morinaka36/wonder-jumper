export enum ObjectType {
    COLLISION = 'collision',
    SPAWN_PLAYER = 'spawn_player',
    SPAWN_ENEMY = 'spawn_enemy',
    TILE = 'tile',
    DEATH = 'death',
    UNMAPPED = 'unmapped',
}
