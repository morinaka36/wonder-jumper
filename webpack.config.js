const { merge } = require('webpack-merge');
const commonConfig = require('./webpack/webpack.common');

require('dotenv').config();

module.exports = (env, argv) => {
    const prodConfig = () => merge(commonConfig, require('./webpack/webpack.prod'), { env });
    const developConfig = () => merge(commonConfig, require('./webpack/webpack.develop'), { env });

    if (env === 'prod') {
        return prodConfig();
    } else if (env === 'develop') {
        return developConfig();
    } else if (env === 'mobile') {
        const mainConfig = argv.mode === 'production' ? prodConfig() : developConfig();

        return merge(mainConfig, require('./webpack/webpack.mobile'));
    } else {
        throw new Error(`Could not find config for ${env} env`);
    }
};
